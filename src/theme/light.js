export default {
  primary: "#dcc7c7",
  secondary: "#f2f2f2",
  foreground: "#333",
  third: "#fafafa",
  dark: false
};
