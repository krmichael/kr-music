import React from "react";
import dark from "./dark";

export default React.createContext({
  theme: JSON.parse(localStorage.getItem("@krmusicTheme")) || dark
});
