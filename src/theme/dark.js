export default {
  primary: "#110e20",
  secondary: "#1b172f",
  foreground: "#d9d9dc",
  third: "#282246",
  dark: true
};
