import React from "react";

import Content from "../../components/Content";
import Header from "../../components/Header";
import NavigationLeft from "../../components/Nav";
import NavigationRight from "../../components/Right";
import Footer from "../../components/Footer";

import { Container } from "./styles";

export default function Home({ toggleTheme }) {
  return (
    <Container>
      <Header toggleTheme={toggleTheme} />

      <NavigationLeft />

      <Content />

      <NavigationRight />

      <Footer />
    </Container>
  );
}
