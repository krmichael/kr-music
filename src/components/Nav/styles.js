import styled from "styled-components";
import { Container as SidebarContainer } from "../Sidebar/styles";

export const Container = styled(SidebarContainer)`
  width: 210px;
  padding-bottom: 70px;

  @media screen and (min-width: 1367px) {
    width: 270px;
  }

  @media screen and (max-width: 1100px) {
    width: 0px;
  }
`;

export const List = styled.ul`
  margin-top: 90px;
  margin-left: 20px;
  list-style: none;
  overflow: auto;
`;

export const ListItem = styled.li`
  height: 35px;
  display: flex;
  align-items: center;
  margin: 25px 0px;
  cursor: pointer;

  &:first-child {
    margin-top: 0px;
  }
`;

export const Item = styled.a`
  text-decoration: none;
  margin-left: 15px;
`;

export const TextSeparated = styled.h3`
  padding-top: 20px;
  font-weight: bolder;
`;
