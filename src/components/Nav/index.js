import React, { useContext } from "react";
import Icon from "@mdi/react";

import {
  mdiAlbum,
  mdiAlarmBell,
  mdiMicrophoneVariant,
  mdiStarFace,
  mdiRayStartEnd
} from "@mdi/js";

import ThemeContext from "../../theme/context";
import { Container, List, ListItem, Item, TextSeparated } from "./styles";

const ITEMS = [
  {
    id: 1,
    label: "Music",
    icon: mdiAlbum
  },
  {
    id: 2,
    label: "Find",
    icon: mdiAlarmBell
  },
  {
    id: 3,
    label: "Singer",
    icon: mdiMicrophoneVariant
  },
  {
    id: 4,
    label: "Collection",
    icon: mdiStarFace
  },
  {
    id: 5,
    label: "Favorite music",
    icon: mdiRayStartEnd
  },
  {
    id: 6,
    label: "Pop music",
    icon: mdiRayStartEnd
  },
  {
    id: 7,
    label: "BGM music",
    icon: mdiRayStartEnd
  }
];

export default function Nav() {
  const { theme } = useContext(ThemeContext);

  return (
    <Container>
      <List>
        {ITEMS.map(item => {
          return (
            item.id <= 4 && (
              <ListItem key={item.id}>
                <Icon
                  path={item.icon}
                  size={1}
                  color={item.id === 1 ? "#09d3ac" : theme.foreground}
                />

                <Item>{item.label}</Item>
              </ListItem>
            )
          );
        })}

        <TextSeparated>Song list</TextSeparated>

        {ITEMS.map(item => {
          return (
            item.id > 4 && (
              <ListItem key={item.id}>
                <Icon
                  path={item.icon}
                  size={1}
                  color={item.id === 1 ? "#09d3ac" : theme.foreground}
                />

                <Item>{item.label}</Item>
              </ListItem>
            )
          );
        })}
      </List>
    </Container>
  );
}
