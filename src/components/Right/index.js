import React, { useContext } from "react";

import Icon from "@mdi/react";
import { mdiPlay, mdiPause } from "@mdi/js";

import ThemeContext from "../../theme/context";

import dualipa from "../../assets/dualipa.jpg";
import havana from "../../assets/havana.jpg";
import akon from "../../assets/akon.jpg";

import {
  Container,
  Fragment,
  Title,
  Scope,
  WeekList,
  WeekItem,
  ScopeSales,
  Card,
  CardArtist,
  ScopeDetails,
  ArtistName,
  SugestUser,
  Price,
  ViewMore
} from "./styles";

export default function Right({ active = true, listening = false }) {
  const { theme } = useContext(ThemeContext);

  return (
    <Container background={theme.primary} position={0}>
      <Fragment>
        <Title>Weekly List</Title>

        <Scope>
          <WeekList>
            <WeekItem>1 Drake-Nice for what</WeekItem>
            <WeekItem active={active}>
              2 Drake-Rockstar
              <Icon
                path={listening ? mdiPause : mdiPlay}
                color={theme.secondary}
                size={1}
              />
            </WeekItem>
            <WeekItem>3 Maroon 5-Wait</WeekItem>
            <WeekItem>4 Rich The Kid-Plug Walk</WeekItem>
            <WeekItem>5 Taylor Swift-Delicate</WeekItem>
            <WeekItem>6 Dua Lipa-New Rules</WeekItem>
            <WeekItem>7 Luke-One Number Away</WeekItem>
          </WeekList>
        </Scope>

        <Title>Sales support</Title>

        <ScopeSales>
          <Card>
            <CardArtist src={dualipa} alt="dualipa photo" />
            <ScopeDetails>
              <ArtistName>Dualipa</ArtistName>
              <SugestUser>Karla Rabello</SugestUser>
              <Price>$20</Price>
            </ScopeDetails>
          </Card>

          <Card>
            <CardArtist src={havana} alt="havana photo" />
            <ScopeDetails>
              <ArtistName>Havana</ArtistName>
              <SugestUser>Karla Rabello</SugestUser>
              <Price>$16</Price>
            </ScopeDetails>
          </Card>

          <Card>
            <CardArtist src={akon} alt="akon photo" />
            <ScopeDetails>
              <ArtistName>Akon</ArtistName>
              <SugestUser>Karla Rabello</SugestUser>
              <Price>$30</Price>
            </ScopeDetails>
          </Card>

          <ViewMore>View all</ViewMore>
        </ScopeSales>
      </Fragment>
    </Container>
  );
}
