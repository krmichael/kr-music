import styled from "styled-components";

import { Container as SidebarContainer } from "../Sidebar/styles";

export const Container = styled(SidebarContainer)`
  @media screen and (max-width: 1100px) {
    width: 0px;
  }
`;

export const Fragment = styled.div`
  margin-top: 100px;
  overflow: auto;
  margin-bottom: 40px;
`;

export const Title = styled.h3`
  margin-bottom: 7px;
  font-weight: 550;
  letter-spacing: 0.5px;
`;

export const Scope = styled.div`
  padding: 10px 10px 10px 5px;
  margin-right: 10px;
  height: 300px;
  overflow: auto;
  margin-bottom: 40px;
`;

export const WeekList = styled.ul`
  list-style-type: none;
`;

export const WeekItem = styled.li.attrs(props => ({
  active: props.active
}))`
  padding: 5px 5px 5px 7px;
  margin-bottom: 10px;
  font-size: 15px;
  border-radius: 3px;
  background: ${props => props.active && "#09d3ac"};
  color: ${props =>
    props.active ? props.theme.secondary : props.theme.foreground};
  display: flex;
  justify-content: space-between;
  align-items: center;
  cursor: pointer;
`;

export const ScopeSales = styled(Scope)`
  height: auto;
  overflow: hidden;
`;

export const Card = styled.div`
  display: flex;
  height: 90px;
  background: ${props => props.theme.secondary};
  margin-bottom: 15px;
`;

export const CardArtist = styled.img`
  width: 100px;
  height: 90px;
`;

export const ScopeDetails = styled.div`
  margin-left: 20px;
`;

export const ArtistName = styled.h3`
  font-size: 16px;
  margin-top: 7px;
`;

export const SugestUser = styled.h3`
  font-size: 15px;
  font-weight: 500;
`;

export const Price = styled.h3`
  margin-top: 25px;
  font-weight: 500;
`;

export const ViewMore = styled.button`
  width: 100%;
  height: 35px;
  margin-top: 20px;
  font-size: 15px;
  font-weight: bold;
  border-radius: 3px;
  background: ${props => props.theme.secondary};
  color: ${props => props.theme.foreground};
  cursor: pointer;
`;
