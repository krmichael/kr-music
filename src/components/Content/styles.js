import styled from "styled-components";
import backgroundImage from "../../assets/background.png";

export const Container = styled.div`
  width: calc(100% - 570px);
  margin-top: 100px;
  margin-left: 240px;
  padding-bottom: 20px;

  @media screen and (min-width: 1367px) {
    width: calc(100% - 630px);
    margin-left: 300px;
  }

  @media screen and (max-width: 1100px) {
    margin-left: 30px;
    width: calc(100% - 60px);
  }
`;

export const Scope = styled.div`
  position: relative;
`;

export const Blured = styled.div`
  height: 300px;
  background-image: url(${backgroundImage});
  background-position: center;
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-size: 100% 100%;
  filter: blur(8px);
  -webkit-filter: blur(8px);
  margin: 0px 20px;

  @media screen and (min-width: 1367px) {
    height: 350px;
  }

  @media screen and (max-width: 580px) {
    height: 250px;
  }
`;

export const ImageBackground = styled.img`
  position: absolute;
  border-radius: 3px;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 400px;
  height: 250px;

  @media screen and (min-width: 1367px) {
    width: 500px;
    height: 300px;
  }

  @media screen and (max-width: 580px) {
    width: 350px;
    height: 200px;
  }
`;

export const ScopeList = styled.div`
  margin: 50px 20px;
`;

export const HeaderList = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 10px;
`;

export const ListTitle = styled.h3`
  font-weight: 550;
`;

export const Select = styled.select`
  width: 100px;
  padding: 5px;
  border-radius: 5px;
  font-weight: bold;
  background: ${props => props.theme.secondary};
  color: ${props => props.theme.foreground};
`;

export const HorizontalList = styled.div`
  overflow: auto;
  white-space: nowrap;
`;

export const ListItem = styled.div`
  display: inline-block;
  width: 120px;
  height: 170px;
  margin-right: 10px;
  background: ${props => props.theme.secondary};
`;

export const ListArtist = styled(ListItem)`
  background: ${props => props.theme.primary};
  width: 120px;
  height: 150px;
  margin-top: 5px;
`;

export const ScopeArtist = styled.div`
  display: flex;
  width: 120px;
  height: 150px;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
`;

export const ArtistPhoto = styled.img`
  width: 100px;
  height: 100px;
  border: 0.5px solid #09d3ac;
  border-radius: 50%;
  margin-top: 7px;
`;

export const ArtistName = styled.h3`
  margin-bottom: 10px;
  font-weight: 500;
  font-size: 16px;
`;
