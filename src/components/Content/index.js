import React from "react";

import {
  Container,
  Scope,
  Blured,
  ImageBackground,
  ScopeList,
  HeaderList,
  ListTitle,
  Select,
  HorizontalList,
  ListItem,
  ScopeArtist,
  ListArtist,
  ArtistPhoto,
  ArtistName
} from "./styles";

import imageBackground from "../../assets/background.png";
import me from "../../assets/me.png";
import havana from "../../assets/havana.jpg";
import dualipa from "../../assets/dualipa.jpg";
import marron5 from "../../assets/marron5.jpg";
import look from "../../assets/look.jpg";
import akon from "../../assets/akon.jpg";

export default function Content() {
  return (
    <Container>
      <Scope>
        <Blured />

        <ImageBackground src={imageBackground} alt="background image" />
      </Scope>

      <ScopeList>
        <HeaderList>
          <ListTitle>Recommended album</ListTitle>
          <Select>
            <option>POP</option>
          </Select>
        </HeaderList>

        <HorizontalList>
          <ListItem />
          <ListItem />
          <ListItem />
          <ListItem />
          <ListItem />
          <ListItem />
          <ListItem />
          <ListItem />
          <ListItem />
          <ListItem />
          <ListItem />
          <ListItem />
          <ListItem />
        </HorizontalList>
      </ScopeList>

      <ScopeList>
        <HeaderList>
          <ListTitle>Recommended artist</ListTitle>
        </HeaderList>

        <HorizontalList>
          <ListArtist>
            <ScopeArtist>
              <ArtistPhoto src={me} alt="me" />
              <ArtistName>Krmichael</ArtistName>
            </ScopeArtist>
          </ListArtist>

          <ListArtist>
            <ScopeArtist>
              <ArtistPhoto src={havana} alt="me" />
              <ArtistName>Havana</ArtistName>
            </ScopeArtist>
          </ListArtist>

          <ListArtist>
            <ScopeArtist>
              <ArtistPhoto src={dualipa} alt="me" />
              <ArtistName>Dualipa</ArtistName>
            </ScopeArtist>
          </ListArtist>

          <ListArtist>
            <ScopeArtist>
              <ArtistPhoto src={marron5} alt="me" />
              <ArtistName>Marron5</ArtistName>
            </ScopeArtist>
          </ListArtist>

          <ListArtist>
            <ScopeArtist>
              <ArtistPhoto src={look} alt="me" />
              <ArtistName>Look</ArtistName>
            </ScopeArtist>
          </ListArtist>

          <ListArtist>
            <ScopeArtist>
              <ArtistPhoto src={akon} alt="me" />
              <ArtistName>Akon</ArtistName>
            </ScopeArtist>
          </ListArtist>

          <ListArtist>
            <ScopeArtist>
              <ArtistPhoto src={havana} alt="me" />
              <ArtistName>Havana</ArtistName>
            </ScopeArtist>
          </ListArtist>

          <ListArtist>
            <ScopeArtist>
              <ArtistPhoto src={marron5} alt="me" />
              <ArtistName>Marron5</ArtistName>
            </ScopeArtist>
          </ListArtist>

          <ListArtist>
            <ScopeArtist>
              <ArtistPhoto src={dualipa} alt="me" />
              <ArtistName>Dualipa</ArtistName>
            </ScopeArtist>
          </ListArtist>

          <ListArtist>
            <ScopeArtist>
              <ArtistPhoto src={look} alt="me" />
              <ArtistName>Look</ArtistName>
            </ScopeArtist>
          </ListArtist>

          <ListArtist>
            <ScopeArtist>
              <ArtistPhoto src={akon} alt="me" />
              <ArtistName>Akon</ArtistName>
            </ScopeArtist>
          </ListArtist>

          <ListArtist>
            <ScopeArtist>
              <ArtistPhoto src={me} alt="me" />
              <ArtistName>Krmichael</ArtistName>
            </ScopeArtist>
          </ListArtist>

          <ListArtist>
            <ScopeArtist>
              <ArtistPhoto src={dualipa} alt="me" />
              <ArtistName>Dualipa</ArtistName>
            </ScopeArtist>
          </ListArtist>

          <ListArtist>
            <ScopeArtist>
              <ArtistPhoto src={akon} alt="me" />
              <ArtistName>Akon</ArtistName>
            </ScopeArtist>
          </ListArtist>

          <ListArtist>
            <ScopeArtist>
              <ArtistPhoto src={havana} alt="me" />
              <ArtistName>Havana</ArtistName>
            </ScopeArtist>
          </ListArtist>
        </HorizontalList>
      </ScopeList>
    </Container>
  );
}
