import styled from "styled-components";

export const Container = styled.div`
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 5px;
  bottom: 0px;
  width: 100%;
  background: ${props => props.theme.third};

  @media screen and (min-width: 1367px) {
    padding: 10px;
  }

  @media screen and (max-width: 879px) {
    display: none;
  }
`;

export const Artist = styled.div`
  display: flex;
  margin-left: 50px;
  width: 20%;

  @media screen and (min-width: 1367px) {
    margin-left: 75px;
  }

  @media screen and (max-width: 1100px) {
    margin-left: 10px;
    width: 30%;
  }
`;

export const Scope = styled.div`
  margin-left: 20px;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const ArtistAvatar = styled.img`
  width: 50px;
  height: 50px;
  border-radius: 50%;

  @media screen and (min-width: 1367px) {
    width: 60px;
    height: 60px;
  }
`;

export const Music = styled.h3``;

export const Description = styled.h3`
  margin-top: 10px;
  font-size: 15px;
  font-weight: 500;
  color: ${props => props.theme.foreground};
`;

export const Player = styled.div`
  display: flex;
  align-items: center;
  width: 60%;

  @media screen and (max-width: 1100px) {
    width: 50%;
  }
`;

export const Time = styled.h3`
  margin-right: 15px;
`;

export const Progress = styled.div`
  width: 80%;
  height: 5px;
  border-radius: 5px;
  background: ${props => props.theme.foreground};
`;

export const ProgressRate = styled(Progress)`
  width: 50%;
  background: #09d3ac;
`;

export const Controls = styled.div`
  display: flex;
  width: 30%;
  justify-content: space-between;

  @media screen and (min-width: 1367px) {
    width: 20%;
  }
`;

export const Control = styled.div`
  svg {
    margin: 10px;
    @media screen and (max-width: 1100px) {
      margin: 5px;
    }
  }
`;

export const ControlAudio = styled.div`
  margin-right: 40px;

  svg {
    margin: 10px;

    @media screen and (max-width: 1100px) {
      margin: 5px;
    }
  }
`;
