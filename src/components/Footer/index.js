import React, { useContext } from "react";
import Icon from "@mdi/react";

import {
  mdiPlay,
  mdiSkipPrevious,
  mdiSkipNext,
  mdiVolumeHigh,
  mdiRefresh
} from "@mdi/js";

import ThemeContext from "../../theme/context";

import {
  Container,
  Artist,
  ArtistAvatar,
  Scope,
  Music,
  Description,
  Player,
  Time,
  Progress,
  ProgressRate,
  Controls,
  Control,
  ControlAudio
} from "./styles";

import akon from "../../assets/akon.jpg";

export default function Footer() {
  const { theme } = useContext(ThemeContext);

  return (
    <Container>
      <Artist>
        <ArtistAvatar src={akon} alt="Akon avatar" />
        <Scope>
          <Music>All The Stars</Music>
          <Description>Kendrick Lamar {"&"} SZA</Description>
        </Scope>
      </Artist>

      <Player>
        <Time>2:25</Time>
        <Progress>
          <ProgressRate />
        </Progress>
      </Player>

      <Controls>
        <Control>
          <Icon path={mdiSkipPrevious} size={1.2} color={theme.foreground} />
          <Icon path={mdiPlay} size={1.2} color={theme.foreground} />
          <Icon path={mdiSkipNext} size={1.2} color={theme.foreground} />
        </Control>

        <ControlAudio>
          <Icon path={mdiVolumeHigh} size={1.2} color={theme.foreground} />
          <Icon path={mdiRefresh} size={1.2} color={theme.foreground} />
        </ControlAudio>
      </Controls>
    </Container>
  );
}
