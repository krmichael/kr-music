import styled from "styled-components";

export const Container = styled.div`
  position: fixed;
  width: 100%;
  z-index: 1;
  top: 0;
  padding: 10px 15px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const Logo = styled.div`
  display: flex;
  align-items: center;
  z-index: 2;
`;
export const TextLogo = styled.h1`
  font-size: 20px;
  color: #09d3ac;
  letter-spacing: -1px;
  font-weight: bolder;
`;

export const Search = styled.div`
  display: flex;
  width: 400px;
  height: 40px;
  border-radius: 5px;
  align-items: center;
  background: ${props => props.theme.secondary};

  @media screen and (max-width: 580px) {
    display: none;
  }
`;

export const Input = styled.input`
  width: 100%;
  height: 100%;
  padding-left: 10px;
  background: ${props => props.theme.secondary};
  color: ${props => props.theme.foreground};

  &::placeholder {
    font-weight: bold;
    color: ${props => props.theme.foreground};
  }
`;

export const Settings = styled.div`
  display: flex;
  justify-content: space-between;
  z-index: 2;
`;
export const Profile = styled.div`
  display: flex;
  align-items: center;
`;
export const Avatar = styled.img`
  width: 40px;
  height: 40px;
  border-radius: 50%;
`;
export const ScopeUser = styled.div`
  margin: 0px 7px;
`;
export const UserName = styled.h3`
  font-size: 16px;
`;
export const UserSocial = styled.span`
  font-size: 12px;
  font-weight: bold;
  color: #09d3ac;
`;

export const ToggleThemeButton = styled.button`
  background: inherit;
  cursor: pointer;
  margin-left: 20px;
  margin-right: 15px;
`;
