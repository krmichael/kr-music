import React, { useContext } from "react";
import Icon from "@mdi/react";

import {
  mdiMusicBoxOutline,
  mdiLightbulbOutline,
  mdiWeatherNight,
  mdiMagnify
} from "@mdi/js";

import ThemeContext from "../../theme/context";

import {
  Container,
  Logo,
  TextLogo,
  Search,
  Input,
  Settings,
  Profile,
  Avatar,
  ScopeUser,
  UserName,
  UserSocial,
  ToggleThemeButton
} from "./styles";

import Me from "../../assets/me.png";

export default function Header({ toggleTheme }) {
  const { theme } = useContext(ThemeContext);

  return (
    <Container>
      <Logo>
        <Icon path={mdiMusicBoxOutline} size={2} color="#09d3ac" />
        <TextLogo>Kr Music</TextLogo>
      </Logo>

      <Search>
        <Input placeholder="Search" />
        <Icon
          path={mdiMagnify}
          size={1}
          color={theme.foreground}
          style={{ marginRight: 5, cursor: "pointer" }}
        />
      </Search>

      <Settings>
        <Profile>
          <Avatar src={Me} alt={`Avatar krmichael`} />
          <ScopeUser>
            <UserName>krmichael</UserName>
            <UserSocial>@krmichael</UserSocial>
          </ScopeUser>
        </Profile>

        <ToggleThemeButton onClick={toggleTheme}>
          <Icon
            path={theme.dark ? mdiLightbulbOutline : mdiWeatherNight}
            color={theme.dark ? "#fae33a" : theme.foreground}
            size={1}
            title={`Change theme for ${theme.dark ? "Light" : "Dark"}`}
          />
        </ToggleThemeButton>
      </Settings>
    </Container>
  );
}
