import React from "react";
import PropTypes from "prop-types";

import { Container } from "./styles";

const Sidebar = ({ children, background, position }) => {
  return (
    <Container background={background} position={position}>
      {children}
    </Container>
  );
};

Sidebar.propTypes = {
  children: PropTypes.element.isRequired,
  background: PropTypes.string,
  position: PropTypes.number
};

Sidebar.defaultProps = {
  background: "",
  position: null
};

export default Sidebar;
