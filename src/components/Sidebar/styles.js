import styled from "styled-components";

export const Container = styled.div`
  position: fixed;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  background: ${props => props.background || props.theme.secondary};
  width: 300px;
  height: 100%;
  top: 0px;
  right: ${props => props.position};
`;
