import React, { Component } from "react";
import { ThemeProvider } from "styled-components";

import * as themes from "./theme";
import ThemeContext from "./theme/context";
import GlobalStyle from "./styles/global";

import Home from "./pages/Home";

export default class App extends Component {
  state = {
    theme: JSON.parse(localStorage.getItem("@krmusicTheme")) || themes.dark
  };

  handleToggleTheme = () => {
    const { theme } = this.state;

    this.setState(
      state => ({
        theme: state.theme === themes.dark ? themes.light : themes.dark
      }),
      localStorage.setItem(
        "@krmusicTheme",
        JSON.stringify(theme === themes.dark ? themes.light : themes.dark)
      )
    );
  };

  render() {
    return (
      <ThemeContext.Provider value={this.state}>
        <ThemeContext.Consumer>
          {theme => (
            <ThemeProvider theme={theme.theme}>
              <GlobalStyle />
              <Home toggleTheme={this.handleToggleTheme} />
            </ThemeProvider>
          )}
        </ThemeContext.Consumer>
      </ThemeContext.Provider>
    );
  }
}
