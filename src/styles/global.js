import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    border: none;
    outline: none;
    box-sizing: border-box;
  }

  html, body {
    background-color: ${props => props.theme.primary};
    color: ${props => props.theme.foreground};
    font-family: Arial, Helvetica, sans-serif;
  }

  html, body, #app {
    height: 100%;
  }

  ::-webkit-scrollbar {
    width: 0px;
  }

  ::-webkit-scrollbar-thumb {
    background: transparent;
  }

  ::-webkit-scrollbar-track {
    background: transparent;
  }

  ::-webkit-scrollbar-track-piece {
    background: transparent;
  }
`;
